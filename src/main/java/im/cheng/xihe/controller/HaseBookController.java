package im.cheng.xihe.controller;

import java.time.Duration;

import reactor.core.publisher.Mono;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import im.cheng.xihe.config.XiheConfiguration;
import im.cheng.xihe.service.HaseBookService;

@RestController
public class HaseBookController {
    private final HaseBookService service;
    private final RedisTemplate<String, String> redisTemplate;

    private final XiheConfiguration xiheConfiguration;

    private final String key = "hasebook";

    HaseBookController(HaseBookService service, RedisTemplate<String, String> redisTemplate, XiheConfiguration xiheConfiguration) {
        this.redisTemplate = redisTemplate;

        this.service = service;

        this.xiheConfiguration = xiheConfiguration;
    }

    @GetMapping(value = { "/hasebook", "/hasebook/{department}" }, produces = "text/calendar")
    public Mono<String> index(@PathVariable(required = false) String department) {
        String key = this.key + ":" + (department == null ? "web" : department);

        String data = redisTemplate.opsForValue().get(key);

        return Mono.just(data == null ? "" : data).flatMap(value -> {
            if (value.length() > 0) {
                return Mono.just(value);
            } else {
                return service.fetch(department).map(result -> {
                    redisTemplate.opsForValue().set(key, result, Duration.ofSeconds(xiheConfiguration.getTimeToLive()));

                    return result;
                });
            }
        });
    }
}
