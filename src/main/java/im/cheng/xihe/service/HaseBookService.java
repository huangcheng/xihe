package im.cheng.xihe.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.*;
import net.fortuna.ical4j.util.RandomUidGenerator;

import reactor.core.publisher.Mono;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VTimeZone;

import lombok.Data;
import lombok.Getter;
import lombok.AllArgsConstructor;

@Data
@Getter
@AllArgsConstructor
class Record {
    private String date;
    private String name;
}

@Service
public class HaseBookService {
    private final RestTemplate restTemplate;

    private final WebClient client;

    private final Logger logger = LoggerFactory.getLogger(HaseBookService.class);

    public HaseBookService() {
        this.restTemplate = new RestTemplate();
        this.client = WebClient.builder().baseUrl("http://gd.hasebook.net").build();
    }

    private String toCalendar(List<Record> records) {
        Calendar calendar = new Calendar();

        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        VTimeZone tz = registry.getTimeZone("Asia/Shanghai").getVTimeZone();

        calendar.getComponents().add(tz);

        calendar.getProperties().add(new ProdId("-//xihe//值班表//CN"));
        calendar.getProperties().add(Version.VERSION_2_0);
        calendar.getProperties().add(CalScale.GREGORIAN);
        calendar.getProperties().add(new XProperty("X-WR-CALNAME", "\uD83D\uDCCB值班表"));

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        for (Record record : records) {
            java.util.Calendar cal = java.util.Calendar.getInstance();

            Date date = null;

            try {
                date = format.parse(record.getDate());
            } catch (ParseException e) {
                date = null;

                logger.error(e.getMessage());
            }

            if (date == null) {
                continue;
            }

            cal.setTime(date);

            DateTime start = new DateTime(cal.getTime());

            cal.add(java.util.Calendar.DATE, 1);

            DateTime end = new DateTime(cal.getTime());

            VEvent v = new VEvent(start, end, record.getName());
            v.getProperties().add(tz.getTimeZoneId());

            Uid uid = new RandomUidGenerator().generateUid();

            v.getProperties().add(uid);

            calendar.getComponents().add(v);
        }

        return calendar.toString();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Mono<String> fetch(String department) throws HttpServerErrorException.InternalServerError {
        String[] departments = {"web", "iaas", "app", "k8s", "stor", "cmp", "iot", "bigdata", "billing"};
        List<String> whiteLists = Arrays.stream(departments).toList();

        if (department != null && !whiteLists.contains(department)) {
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "department not found");
        }

        return client.get()
                .uri("/{department}", department == null ? "web" : department)
                .retrieve()
                .bodyToMono(String.class)
                .map(data -> data.split("\n"))
                .map(lines -> {
                    Pattern pattern = Pattern.compile("(\\d{4}-\\d{2}-\\d{2}) *\\d+(.+)");

                    List<Record> records = new ArrayList<>(lines.length - 1);

                    for (int i = 1; i < lines.length; i++) {
                        Matcher m = pattern.matcher(lines[i]);

                        if (m.find()) {
                            String date = m.group(1).trim();
                            String name = m.group(2).trim();

                            if (name.contains(",")) {
                                String[] names = name.split(",");

                                for (String n : names) {
                                    records.add(new Record(date, n.trim()));
                                }
                            } else {
                                records.add(new Record(date, name));
                            }
                        }
                    }

                    return records;
                }).map(this::toCalendar);
    }
}
